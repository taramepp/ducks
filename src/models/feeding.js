// class to track duck feedings
export class Feeding {
    constructor(        
        amount,
        ducksFed,
        foodType,
        latitude,
        longitude,
        time,
    ) {
        this.amount = amount;
        this.ducksFed = ducksFed;
        this.foodType = foodType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.time = time;
        this.id = Math.random();
    }

    /**
     * takes the doc as returned by the database,
     * and turns it into an object
     * @param {*} input 
     * @returns 
     */
    deserialize(input) {
        // fix the time in here
        const time = new Date(input.time).toLocaleString();
        let temp = input;
        temp.time = time;
        return Object.assign(this, temp);
    }
}