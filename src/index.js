import React from 'react';
import ReactDOM from 'react-dom';
import * as fb from './firebase';
import './index.css';

class FeedingForm extends React.Component {
  constructor(props) {
    super(props);

    const now = Date.now();

    this.state = {
      time: now,
      foodType: "",
      amount: 0,
      latitude: 0,
      longitude: 0,
      ducksFed: "1",
      history: [],
      foodTypes: []
    };

    this.labels = {
      time: "Time & Date Fed",
      foodType: "Type of Food",
      amount: "Amount (grams)",
      latitude: "Latitude",
      longitude: "Longitude",
      ducksFed: "Number of ducks fed"
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    // send a callback to the subscribe method so that it updates
    // the state whenever new data is added
    fb.getFeeds((feedings) => {
      this.setState({ history: feedings});
    });
    
  }

  render() {
    let form = this.getForm();
    let history = this.getHistory();
    return (
      <div className='container'>
        <h1>Welcome to Duck Tracker</h1>
        <div className='card bg-light'>
          <div className='card-body'>
            <h2 className='card-title'>Record a Duck Feeding</h2>
            {form}
            
          </div>
        </div>
        {history}
      </div>     
      );
  }

  /**
   * 
   * @returns div containing all existing records in cards
   */
  getHistory() {
    const history = this.state.history;
    // loop through each history item, put in contaienr,
    // loop through each displayed label, output corresponding attribute
    return (
      <div id='history'>
        <h2>Duck Feeding History</h2>
        {history.map(record => {
          return (
            <div key={"history" + record.id} className='card bg-dark'>
              <div className='card-body'>
                <ul className='list-group'>

                  {Object.entries(this.labels).map((entry) => {
                    const label = entry[1];
                    const stateKey = entry[0];
                    return (
                      <li className='list-group-item' key={stateKey + "LI" + record.uid}>{label}: {record[stateKey]}</li>
                    );
                  })}
                </ul>
              </div>
            </div>
          );
        })}
      </div>
    );
  }

  // the meat and bones of the page
  getForm() {
    // all of our form content
    let controls = [
      {key: "foodType", content: [this.getInput("text", "foodType")]},
      {key: "amount", content: [this.getInput("number", "amount")]},
      {key: "latitude", content: [this.getInput("number", "latitude")]},
      {key: "longitude", content: [this.getInput("number", "longitude"),
      <button key='mapSupportLinkButton'
        onClick={() => window.open('https://support.google.com/maps/answer/18539')}
        className='btn btn-info'>How to find your location</button>]},
      {key: "ducksFed", content: [this.getInput("number", "ducksFed")]}
    ];

    // form element, loop through all controls,
    // create a label, output all associated elements
    // associate label to element with state key
    return (
      <form onSubmit={this.handleSubmit}>
        {controls.map(control => {
          return (
            <div key={control.key + "Outer"}>
              <label htmlFor={control.key}>{this.labels[control.key]}</label> 
              {control.content.map(element => {return element})}
            </div>          
          );
        })}
        <button onClick={this.handleSubmit} className='btn btn-primary' type="button">Submit</button>        
      </form>
    );
  }

  // create an input to match a state attribute
  getInput(type, stateKey=false) {
    return (
      <input 
        className='form-control'
        type={type}
        name={stateKey}
        key={stateKey + "FormControl"}
        id={stateKey}
        value={this.state[stateKey]}
        onChange={this.handleChange}
      />
    );
  }

  /**
   * updates the state
   * uses target's name for key
   * value for new value
   * @param {*} event 
   */
  handleChange(event) {
    const stateKey = event.target.name;
    const newValue = event.target.value;
    this.setState({[stateKey]: newValue});
  }

  /**
   * create a new record on submit
   */
  handleSubmit() {
    const newFeeding = {
      time: this.state.time,
      foodType: this.state.foodType,
      amount: this.state.amount,
      latitude: this.state.latitude,
      longitude: this.state.longitude,
      ducksFed: this.state.ducksFed
    }

    fb.addFeeding(newFeeding).then((docRef) => {
      if (docRef) {
        console.log("New id is ", docRef.id);
      } else {
        console.log("failed")
      }
    });   
    
  }
}
  
  // ========================================
  
  ReactDOM.render(
    <React.StrictMode>
    <FeedingForm />
  </React.StrictMode>,
    document.getElementById('root')
  );
  