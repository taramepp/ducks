import { initializeApp } from "firebase/app";
import { getFirestore, collection, addDoc } from "firebase/firestore";
import { Feeding } from "./models/feeding";
import { collectionData } from 'rxfire/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAI4c0mTzB4PoxJXEWkh0jHAygp8WyoZH0",
    authDomain: "ducks-d3c48.firebaseapp.com",
    projectId: "ducks-d3c48",
    storageBucket: "ducks-d3c48.appspot.com",
    messagingSenderId: "476046216017",
    appId: "1:476046216017:web:4f66a24c1d46be3726a170",
    measurementId: "G-KNQWJJJSD2"
  };

// initialize firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(); 

const feedingsCol = collection(db, "Feedings");

// export functions to keep db code out of frontend

/**
 * gets history of feedings.
 * Performs the callback function on the data that comes back.
 * @returns nothing
 */
export const getFeeds = (callback) => {
    // I lost the id in this process... oh well. Could put it back if it was important
    collectionData(feedingsCol).subscribe(docs => {
        docs = docs.map(doc => new Feeding().deserialize(doc));
        callback(docs);
    });
};

// keep collections details in this file-
export const addFeeding = async (recordData) => {
    return await addDocument(feedingsCol, recordData);
};

// general purpose document add-er
const addDocument = async (collection, recordData) => {
    try {
        const docRef = await addDoc(collection, recordData);
        return docRef;
    } catch (e) {
        console.log(e);
        return false;
    }
};


