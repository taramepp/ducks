How to Install:
  Prerequisites:
  - install/update nodeJS: https://nodejs.org/en/
  - run npm install to install dependencies

How to Run:
- in the directory, run npm start
- a browser window will open with the web page

Node Libraries used:
firebase
firebase-tools
rxjs
rxfire